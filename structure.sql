CREATE TABLE restaurant (
  id            INT AUTO_INCREMENT NOT NULL,
  PRIMARY KEY (id)
)
  DEFAULT CHARACTER SET utf8
  COLLATE utf8_czech_ci
  ENGINE = InnoDB;

CREATE TABLE holiday (
  id            INT AUTO_INCREMENT NOT NULL,
  restaurant_id INT DEFAULT NULL,
  day           DATE               NOT NULL,
  INDEX IDX_DC9AB234B1E7706E (restaurant_id),
  PRIMARY KEY (id)
)
  DEFAULT CHARACTER SET utf8
  COLLATE utf8_czech_ci
  ENGINE = InnoDB;

CREATE TABLE opening_hours (
  id            INT AUTO_INCREMENT NOT NULL,
  restaurant_id INT DEFAULT NULL,
  day_of_week   TINYINT            NOT NULL,
  start         TIME               NOT NULL,
  stop          TIME               NOT NULL,
  INDEX IDX_2640C10BB1E7706E (restaurant_id),
  PRIMARY KEY (id)
)
  DEFAULT CHARACTER SET utf8
  COLLATE utf8_czech_ci
  ENGINE = InnoDB;

CREATE TABLE temporary_stop (
  id            INT AUTO_INCREMENT NOT NULL,
  restaurant_id INT DEFAULT NULL,
  start         DATETIME           NOT NULL,
  stop          DATETIME           NOT NULL,
  INDEX IDX_EE080BF1B1E7706E (restaurant_id),
  PRIMARY KEY (id)
)
  DEFAULT CHARACTER SET utf8
  COLLATE utf8_czech_ci
  ENGINE = InnoDB;

ALTER TABLE holiday ADD CONSTRAINT FK_DC9AB234B1E7706E FOREIGN KEY (restaurant_id) REFERENCES restaurant (id);
ALTER TABLE opening_hours ADD CONSTRAINT FK_2640C10BB1E7706E FOREIGN KEY (restaurant_id) REFERENCES restaurant (id);
ALTER TABLE temporary_stop ADD CONSTRAINT FK_EE080BF1B1E7706E FOREIGN KEY (restaurant_id) REFERENCES restaurant (id);


CREATE TABLE pregenerated_opening (
  id            INT AUTO_INCREMENT NOT NULL,
  restaurant_id INT DEFAULT NULL,
  date          DATE               NOT NULL,
  start         TIME               NOT NULL,
  stop          TIME               NOT NULL,
  INDEX IDX_1AECB32CB1E7706E (restaurant_id),
  INDEX idx_pregenerated_opening_date (date),
  INDEX idx_pregenerated_opening_start (start),
  INDEX idx_pregenerated_opening_stop (stop),
  PRIMARY KEY (id)
)
  DEFAULT CHARACTER SET utf8
  COLLATE utf8_czech_ci
  ENGINE = InnoDB;

ALTER TABLE pregenerated_opening ADD CONSTRAINT FK_1AECB32CB1E7706E FOREIGN KEY (restaurant_id) REFERENCES restaurant (id);
