# Open Restaurants

## Zadání

Cílem je vytvořit model, který dokáže rychle vybrat restaurace, které mají právě otevřeno, pokud je o každé z nich známo:

- Otevírací doba pro každý den v týdnu. Pro každý den může mít restaurace více intervalů.
- Svátky - datumy dní, kdy má restaurace mimořádně zavřeno.
- Pozastavení příjmu objednávek. Čas, do kdy restaurace pozastavuje příjem nových objednávek.

## Způsob řešení

Ze všech tří zdrojů se vygenerují data do jedné tabulky `pregenerated_opening`, kde jsou pouze ID restaurací a časy.

Je na to služba `OpenRestaurants\Pregenerating\Generator`, která by byla spouštěna jednak pravidelně cronem (`app/console open-restaurants:generate`) a jednak při jakýchkoli změnách ve zdrojových entitách, asi přes life-cycle eventy entit (to jsem ale ještě nestihl implementovat). Přegenerování lze omezit časem a ID restaurace, takže při dočasném pozastavení objednávek by se generoval pouze jeden den a pouze u dotyčného podniku.

Dalším cronem by se mohly mazat historické záznamy (to jsem taky nestihl udělat), aby ta tabulka byla pokud možno co nejmenší.

## Instalace a konfigurace

- Zkopírovat a upravit `app/config/config.local.template.neon` do `app/config/config.local.neon`.
- `composer install`
- Nahrát databázi - `structure.sql` a `test-data.sql` (pro testy není potřeba, vytváří si vlastní).

## Konzole

Konzolové příkazy se pouští přes `app/console`:

- `app/console open-restaurants:generate`
- `app/console open-restaurants:list` 

## Spouštění testů

Všechny testy se dají spustit skriptem `tests/run-all-tests.sh`.
