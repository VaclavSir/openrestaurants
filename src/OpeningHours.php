<?php

namespace OpenRestaurants;

use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\Attributes\Identifier;
use Kdyby\Doctrine\Entities\BaseEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @property int $dayOfWeekA
 * @property \DateTime $start
 * @property \DateTime $stop
 * @property Restaurant $restaurant
 *
 * @ORM\Entity
 * @ORM\Table(name="opening_hours")
 */
class OpeningHours extends BaseEntity
{

	use Identifier;

	/**
	 * Numeric day of the week - 1 = Monday, 7 = Sunday (`date('N')`).
	 * @var int
	 * @ORM\Column(type="integer", name="day_of_week", columnDefinition="TINYINT NOT NULL")
	 * @Assert\NotBlank
	 * @Assert\Range(min=1, max=7)
	 */
	protected $dayOfWeek;

	/**
	 * @var \DateTime
	 * @ORM\Column(type="time", nullable=false)
	 * @Assert\NotBlank
	 */
	protected $start;

	/**
	 * @var \DateTime
	 * @ORM\Column(type="time", nullable=false)
	 * @Assert\NotBlank
	 */
	protected $stop;

	/**
	 * @var Restaurant
	 * @ORM\ManyToOne(targetEntity="Restaurant", inversedBy="openingHours")
	 */
	protected $restaurant;

}
