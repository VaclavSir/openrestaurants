<?php

namespace OpenRestaurants\Console;

use Doctrine\ORM\EntityManager;
use Kdyby\Doctrine\EntityDao;
use OpenRestaurants\Query\OpenRestaurantsOptimizedQuery;
use OpenRestaurants\Restaurant;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ListOpenRestaurantsCommand extends Command
{
	/** @var EntityDao */
	private $restaurantDao;

	public function __construct(EntityManager $em, $name = null)
	{
		parent::__construct($name);
		$this->restaurantDao = $em->getRepository(Restaurant::class);
	}

	protected function configure()
	{
		$this->setName('open-restaurants:list')
			->setDescription('Vypíše otevřené restaurace.')
			->addOption('now', null, InputOption::VALUE_OPTIONAL, 'Datum a čas, ve kterém se bude hledat.', 'now');
	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$now = new \DateTime($input->getOption('now'));
		$query = new OpenRestaurantsOptimizedQuery($now);
		$openRestaurants = $this->restaurantDao->fetch($query);
		/** @var Restaurant $restaurant */
		foreach ($openRestaurants as $restaurant) {
			$output->writeln($restaurant->id);
		}
		return 0;
	}

}
