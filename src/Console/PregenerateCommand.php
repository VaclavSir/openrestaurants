<?php

namespace OpenRestaurants\Console;

use Kdyby\Doctrine\EntityManager;
use OpenRestaurants\Pregenerating\Generator;
use OpenRestaurants\Restaurant;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class PregenerateCommand extends Command
{

	/** @var EntityManager */
	private $em;

	/** @var Generator */
	private $generator;

	public function __construct(EntityManager $em, Generator $generator, $name = null)
	{
		parent::__construct($name);
		$this->em = $em;
		$this->generator = $generator;
	}

	protected function configure()
	{
		$this->setName('open-restaurants:generate')
			->setDescription('Vygeneruje aktuální otevírací doby (podle otvíračky, svátků a přerušení).')
			->addOption('dateStart', null, InputOption::VALUE_OPTIONAL, 'Počáteční datum.', 'today')
			->addOption('dateEnd', null, InputOption::VALUE_OPTIONAL, 'Konečné datum.', '+2 days');
	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$dateStart = new \DateTime($input->getOption('dateStart'));
		$dateEnd = new \DateTime($input->getOption('dateEnd'));
		$restaurants = $this->em->getRepository(Restaurant::class)->findAll();
		$this->generator->generate($restaurants, $dateStart, $dateEnd);
		$output->writeln(sprintf('Přegenerovány otevírací doby od %s do %s včetně.', $dateStart->format('j.n.Y'), $dateEnd->format('j.n.Y')));
	}

}
