<?php

namespace OpenRestaurants;

use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\Attributes\Identifier;
use Kdyby\Doctrine\Entities\BaseEntity;

/**
 * @property Restaurant $restaurant
 * @property \DateTime $date
 * @property \DateTime $start
 * @property \DateTime $stop
 *
 * @ORM\Entity
 * @ORM\Table(name="pregenerated_opening")
 */
class PregeneratedOpening extends BaseEntity
{

	use Identifier;

	/**
	 * @var \DateTime
	 * @ORM\Column(type="date")
	 */
	protected $date;

	/**
	 * @var Restaurant
	 * @ORM\ManyToOne(targetEntity="Restaurant", inversedBy="pregeneratedOpenings")
	 */
	protected $restaurant;

	/**
	 * @var \DateTime
	 * @ORM\Column(type="time")
	 */
	protected $start;

	/**
	 * @var \DateTime
	 * @ORM\Column(type="time")
	 */
	protected $stop;

}
