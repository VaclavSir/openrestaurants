<?php

namespace OpenRestaurants;

use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\Attributes\Identifier;
use Kdyby\Doctrine\Entities\BaseEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @property \DateTime $start
 * @property \DateTime $stop
 * @property Restaurant $restaurant
 *
 * @ORM\Entity
 * @ORM\Table(name="temporary_stop")
 */
class TemporaryStop extends BaseEntity
{

	use Identifier;

	/**
	 * @var \DateTime
	 * @ORM\Column(type="datetime", nullable=false)
	 * @Assert\NotBlank
	 */
	protected $start;

	/**
	 * @var \DateTime
	 * @ORM\Column(type="datetime", nullable=false)
	 * @Assert\NotBlank
	 */
	protected $stop;

	/**
	 * @var Restaurant
	 * @ORM\ManyToOne(targetEntity="Restaurant", inversedBy="openingHours")
	 */
	protected $restaurant;

}
