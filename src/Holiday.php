<?php

namespace OpenRestaurants;

use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\Attributes\Identifier;
use Kdyby\Doctrine\Entities\BaseEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @property \DateTime $day
 * @property Restaurant $restaurant
 *
 * @ORM\Entity
 * @ORM\Table(name="holiday")
 */
class Holiday extends BaseEntity
{

	use Identifier;

	/**
	 * @var \DateTime
	 * @ORM\Column(type="date", nullable=false)
	 * @Assert\NotBlank
	 */
	protected $day;

	/**
	 * @var Restaurant
	 * @ORM\ManyToOne(targetEntity="Restaurant", inversedBy="openingHours")
	 */
	protected $restaurant;

}
