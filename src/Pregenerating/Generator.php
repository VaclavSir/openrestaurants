<?php

namespace OpenRestaurants\Pregenerating;

use Kdyby\Doctrine\EntityManager;
use OpenRestaurants\Holiday;
use OpenRestaurants\OpeningHours;
use OpenRestaurants\PregeneratedOpening;
use OpenRestaurants\Query\HolidayQuery;
use OpenRestaurants\Query\OpeningHoursQuery;
use OpenRestaurants\Query\PregeneratedOpeningQuery;
use OpenRestaurants\Query\TemporaryStopQuery;
use OpenRestaurants\Restaurant;
use OpenRestaurants\TemporaryStop;

class Generator
{

	/** @var EntityManager */
	private $em;

	public function __construct(EntityManager $em)
	{
		$this->em = $em;
	}

	/**
	 * @param Restaurant[] $restaurants
	 * @param \DateTime $startDate
	 * @param \DateTime $endDate
	 */
	public function generate($restaurants, \DateTime $startDate, \DateTime $endDate)
	{
		$this->em->beginTransaction();
		$this->removeOldData($restaurants, $startDate, $endDate);
		$this->generateNewData($restaurants, $startDate, $endDate);
		$this->em->flush();
		$this->em->commit();
	}

	private function removeOldData($restaurants, \DateTime $startDate, \DateTime $endDate)
	{
		$oldData = $this->em->getRepository(PregeneratedOpening::class)
			->fetch(new PregeneratedOpeningQuery($restaurants, $startDate, $endDate));
		foreach ($oldData as $oldDatum) {
			$this->em->remove($oldDatum);
		}
	}

	/**
	 * @param $restaurants
	 * @param \DateTime $startDate
	 * @param \DateTime $endDate
	 */
	private function generateNewData($restaurants, \DateTime $startDate, \DateTime $endDate)
	{
		$endDate->modify('+1 day');
		$period = new \DatePeriod($startDate, \DateInterval::createFromDateString('1 day'), $endDate);
		foreach ($period as $date) {
			foreach ($restaurants as $restaurant) {
				$openingHours = $this->em->getRepository(OpeningHours::class)->fetch(new OpeningHoursQuery($date->format('N'), $restaurant))->toArray();
				$holidays = $this->em->getRepository(Holiday::class)->fetch(new HolidayQuery($date, $restaurant))->toArray();
				$temporaryStops = $this->em->getRepository(TemporaryStop::class)->fetch(new TemporaryStopQuery($date, $restaurant))->toArray();
				$openings = SingleDayGenerator::generateOpenings($restaurant, $openingHours, $holidays, $temporaryStops);
				array_walk($openings, function (PregeneratedOpening $opening) use ($date) {
					$opening->date = $date;
					$this->em->persist($opening);
				});
			}
		}
	}

}
