<?php

namespace OpenRestaurants\Pregenerating;

use OpenRestaurants\Holiday;
use OpenRestaurants\OpeningHours;
use OpenRestaurants\PregeneratedOpening;
use OpenRestaurants\Restaurant;
use OpenRestaurants\TemporaryStop;

class SingleDayGenerator
{
	private static $date = '2000-01-01 ';

	/**
	 * @param Restaurant $restaurant
	 * @param OpeningHours[] $openingHours
	 * @param Holiday[] $holidays
	 * @param TemporaryStop[] $temporaryStops
	 * @return PregeneratedOpening[]
	 */
	public static function generateOpenings(Restaurant $restaurant, array $openingHours, array $holidays, array $temporaryStops)
	{
		if ($holidays) {
			return [];
		}

		$openingIntervals = self::processOpeningHours($openingHours);
		$openingIntervals = self::processTemporaryStops($openingIntervals, $temporaryStops);

		return array_map(function (array $interval) use ($restaurant) {
			$opening = new PregeneratedOpening;
			$opening->restaurant = $restaurant;
			$opening->start = $interval[0];
			$opening->stop = $interval[1];
			return $opening;
		}, $openingIntervals);
	}

	private static function processOpeningHours(array $openingHours)
	{
		$openingIntervals = [];
		$openingIntervalsStrings = array_map(function (OpeningHours $openingHours) {
			return $openingHours->start->format('H:i:s-') . $openingHours->stop->format('H:i:s');
		}, $openingHours);
		sort($openingIntervalsStrings);

		$lastStart = $lastStop = null;
		foreach ($openingIntervalsStrings as $intervalString) {
			list($start, $stop) = explode('-', $intervalString);
			$start = new \DateTime(self::$date . $start);
			$stop = new \DateTime(self::$date . $stop);
			if ($start <= $lastStop) {
				array_pop($openingIntervals);
				array_push($openingIntervals, [$lastStart, $stop]);
				$lastStop = $stop;
			} else {
				array_push($openingIntervals, [$start, $stop]);
				$lastStart = $start;
				$lastStop = $stop;
			}
		}
		return $openingIntervals;
	}

	private static function processTemporaryStops($openingIntervals, $temporaryStops)
	{
		$stoppingIntervals = array_map(function (TemporaryStop $temporaryStop) {
			return [
				new \DateTime(self::$date . $temporaryStop->start->format('H:i:s')),
				new \DateTime(self::$date . $temporaryStop->stop->format('H:i:s')),
			];
		}, $temporaryStops);

		foreach ($stoppingIntervals as $stoppingInterval) {
			$openingIntervalsNew = [];
			foreach ($openingIntervals as $openingInterval) {
				$isStoppingStartInsideOpening = ($stoppingInterval[0] >= $openingInterval[0] && $stoppingInterval[0] <= $openingInterval[1]);
				$isStoppingStopInsideOpening = ($stoppingInterval[1] >= $openingInterval[0] && $stoppingInterval[1] <= $openingInterval[1]);
				$isStoppingOverlappingOpening = ($stoppingInterval[0] < $openingInterval[0] && $stoppingInterval[1] > $openingInterval[1]);
				if ($isStoppingStartInsideOpening && $isStoppingStopInsideOpening) {
					//  +--------+        +---+----+---+    +--------+       +---+----+---+
					//  |        |        |   |    |   |    |        |       |   |    |   |
					//  |  +---------+    |   +--------+    |        |       |   |    |   |
					//  |  |     |   |    |        |        |  +---------+   |   |    |   |
					//  |  +---------+    |        |        |  |     |   |   |   |    |   |
					//  +--+-----+        +--------+        +--+-----+---+   +---+----+---+
					if ($stoppingInterval[0] != $openingInterval[0]) {
						$openingIntervalsNew[] = [$openingInterval[0], $stoppingInterval[0]];
					}
					if ($stoppingInterval[1] != $openingInterval[1]) {
						$openingIntervalsNew[] = [$stoppingInterval[1], $openingInterval[1]];
					}
				} elseif ($isStoppingStartInsideOpening) {
					//   +--------+
					//   |        |
					//   |        |
					//   |  +--------+
					//   +--------|  |
					//      +-----+--+
					$openingIntervalsNew[] = [$openingInterval[0], $stoppingInterval[0]];
				} elseif ($isStoppingStopInsideOpening) {
					//      +----+---+
					//  +--------|   |
					//  |   +--------+
					//  |        |
					//  |        |
					//  +--------+
					$openingIntervalsNew[] = [$stoppingInterval[1], $openingInterval[1]];
				} elseif ($isStoppingOverlappingOpening) {
					//      +----+---+
					//  +--------+   |
					//  |   |    |   |
					//  |   |    |   |
					//  |   |    |   |
					//  +--------+   |
					//      +----+---+
				} else {
					$openingIntervalsNew[] = $openingInterval;
				}
			}
			$openingIntervals = $openingIntervalsNew;
		}

		return $openingIntervals;
	}

}
