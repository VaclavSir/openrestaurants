<?php

namespace OpenRestaurants\Query;

use Kdyby;
use Kdyby\Doctrine\QueryObject;
use OpenRestaurants\Restaurant;

class OpeningHoursQuery extends QueryObject
{

	/** @var int */
	private $dayOfWeek;

	/** @var Restaurant */
	private $restaurant;

	function __construct($dayOfWeek = NULL, $restaurant = NULL)
	{
		$this->dayOfWeek = $dayOfWeek;
		$this->restaurant = $restaurant;
	}

	/**
	 * @param \Kdyby\Persistence\Queryable $repository
	 * @return \Doctrine\ORM\Query|\Doctrine\ORM\QueryBuilder
	 */
	protected function doCreateQuery(Kdyby\Persistence\Queryable $repository)
	{
		$queryBuilder = $repository->createQueryBuilder('openingHours');
		if ($this->dayOfWeek !== NULL) {
			$queryBuilder->andWhere('openingHours.dayOfWeek = :dayOfWeek');
			$queryBuilder->setParameter(':dayOfWeek', $this->dayOfWeek);
		}
		if ($this->restaurant !== NULL) {
			$queryBuilder->andWhere('openingHours.restaurant = :restaurant');
			$queryBuilder->setParameter(':restaurant', $this->restaurant);
		}
		return $queryBuilder;
	}

	/**
	 * @param int $dayOfWeek
	 */
	public function setDayOfWeek($dayOfWeek)
	{
		$this->dayOfWeek = $dayOfWeek;
	}

	/**
	 * @param Restaurant $restaurant
	 */
	public function setRestaurant($restaurant)
	{
		$this->restaurant = $restaurant;
	}

}
