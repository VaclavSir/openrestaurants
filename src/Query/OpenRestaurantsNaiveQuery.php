<?php

namespace OpenRestaurants\Query;

use Kdyby;
use Kdyby\Doctrine\QueryObject;

class OpenRestaurantsNaiveQuery extends QueryObject
{

	/** @var \DateTime */
	private $currentTime;

	public function __construct(\DateTime $currentTime)
	{
		$this->currentTime = $currentTime;
	}

	/**
	 * @param \Kdyby\Persistence\Queryable $repository
	 * @return \Doctrine\ORM\Query|\Doctrine\ORM\QueryBuilder
	 */
	protected function doCreateQuery(Kdyby\Persistence\Queryable $repository)
	{
		$queryBuilder = $repository->createQueryBuilder('restaurant');
		$queryBuilder->join('restaurant.openingHours', 'openingHours')
			->leftJoin('restaurant.holidays', 'holiday')
			->leftJoin('restaurant.temporaryStops', 'temporaryStop')
			->where('openingHours.dayOfWeek = :dayOfWeek')
			->andWhere('openingHours.start <= :currentTime')
			->andWhere('openingHours.stop >= :currentTime')
			->andWhere('(holiday.id IS NULL OR holiday.day <> :currentDate)')
			->andWhere('temporaryStop.id IS NULL OR NOT (temporaryStop.start <= :currentDateTime AND temporaryStop.stop >= :currentDateTime)')
			->setParameter(':currentDateTime', $this->currentTime)
			->setParameter(':currentDate', $this->currentTime->format('Y-m-d'))
			->setParameter(':currentTime', $this->currentTime->format('H:i:s'))
			->setParameter(':dayOfWeek', $this->currentTime->format('N'));
		return $queryBuilder;
	}

}
