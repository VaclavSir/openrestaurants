<?php

namespace OpenRestaurants\Query;

use Kdyby;
use Kdyby\Doctrine\QueryObject;
use OpenRestaurants\Restaurant;

class HolidayQuery extends QueryObject
{

	/** @var \DateTime */
	private $date;

	/** @var Restaurant */
	private $restaurant;

	function __construct(\DateTime $date = NULL, $restaurant = NULL)
	{
		$this->date = $date;
		$this->restaurant = $restaurant;
	}

	/**
	 * @param \Kdyby\Persistence\Queryable $repository
	 * @return \Doctrine\ORM\Query|\Doctrine\ORM\QueryBuilder
	 */
	protected function doCreateQuery(Kdyby\Persistence\Queryable $repository)
	{
		$queryBuilder = $repository->createQueryBuilder('holiday');
		if ($this->date !== NULL) {
			$queryBuilder->andWhere('holiday.day = :date');
			$queryBuilder->setParameter(':date', $this->date->format('Y-m-d'));
		}
		if ($this->restaurant !== NULL) {
			$queryBuilder->andWhere('holiday.restaurant = :restaurant');
			$queryBuilder->setParameter(':restaurant', $this->restaurant);
		}
		return $queryBuilder;
	}

	/**
	 * @param Restaurant $restaurant
	 */
	public function setRestaurant($restaurant)
	{
		$this->restaurant = $restaurant;
	}

	/**
	 * @param \DateTime $date
	 */
	public function setDate(\DateTime $date)
	{
		$this->date = $date;
	}

}
