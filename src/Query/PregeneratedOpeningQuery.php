<?php

namespace OpenRestaurants\Query;

use Kdyby;
use Kdyby\Doctrine\QueryObject;
use OpenRestaurants\Restaurant;

class PregeneratedOpeningQuery extends QueryObject
{

	/** @var Restaurant[] */
	private $restaurants;

	/** @var \DateTime */
	private $dateMin;

	/** @var \DateTime */
	private $dateMax;

	public function __construct($restaurants, $dateMin, $dateMax)
	{
		$this->restaurants = $restaurants;
		$this->dateMin = $dateMin;
		$this->dateMax = $dateMax;
	}

	/**
	 * @param \Kdyby\Persistence\Queryable $repository
	 * @return \Doctrine\ORM\Query|\Doctrine\ORM\QueryBuilder
	 */
	protected function doCreateQuery(Kdyby\Persistence\Queryable $repository)
	{
		return $repository->createQueryBuilder('opening')
			->andWhere('opening.restaurant IN (:restaurants)')
			->andWhere('opening.date >= :dateMin')
			->andWhere('opening.date <= :dateMax')
			->setParameters([
				':dateMin' => $this->dateMin->format('Y-m-d'),
				':dateMax' => $this->dateMax->format('Y-m-d'),
			])
			->setParameter(':restaurants', $this->restaurants, \Doctrine\DBAL\Types\Type::TARRAY);
	}

}
