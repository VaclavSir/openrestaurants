<?php

namespace OpenRestaurants\Query;

use Kdyby;
use Kdyby\Doctrine\QueryObject;

class OpenRestaurantsOptimizedQuery extends QueryObject
{

	/** @var \DateTime */
	private $currentTime;

	public function __construct(\DateTime $currentTime)
	{
		$this->currentTime = $currentTime;
	}

	/**
	 * @param \Kdyby\Persistence\Queryable $repository
	 * @return \Doctrine\ORM\Query|\Doctrine\ORM\QueryBuilder
	 */
	protected function doCreateQuery(Kdyby\Persistence\Queryable $repository)
	{
		$queryBuilder = $repository->createQueryBuilder('restaurant');
		$queryBuilder->innerJoin('restaurant.pregeneratedOpenings', 'opening')
			->andWhere('opening.date = :currentDate')
			->andWhere('opening.start <= :currentTime')
			->andWhere('opening.stop > :currentTime')
			->setParameter(':currentDate', $this->currentTime->format('Y-m-d'))
			->setParameter(':currentTime', $this->currentTime->format('H:i:s'));
		return $queryBuilder;
	}

}
