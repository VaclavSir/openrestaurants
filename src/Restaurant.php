<?php

namespace OpenRestaurants;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\Attributes\Identifier;
use Kdyby\Doctrine\Entities\BaseEntity;

/**
 * @property-read OpeningHours[] $openingHours
 * @method addOpeningHour(OpeningHours $openingHours)
 * @method removeOpeningHour(OpeningHours $openingHours)
 *
 * @ORM\Entity
 * @ORM\Table(name="restaurant")
 */
class Restaurant extends BaseEntity
{

	use Identifier;

	/**
	 * @var OpeningHours[]|Collection
	 * @ORM\OneToMany(targetEntity="OpeningHours", mappedBy="restaurant")
	 */
	protected $openingHours;

	/**
	 * @var Holiday[]|Collection
	 * @ORM\OneToMany(targetEntity="Holiday", mappedBy="restaurant")
	 */
	protected $holidays;

	/**
	 * @var TemporaryStop[]|Collection
	 * @ORM\OneToMany(targetEntity="TemporaryStop", mappedBy="restaurant")
	 */
	protected $temporaryStops;

	/**
	 * @var PregeneratedOpening[]|Collection
	 * @ORM\OneToMany(targetEntity="PregeneratedOpening", mappedBy="restaurant")
	 */
	protected $pregeneratedOpenings;

	public function __construct()
	{
		$this->openingHours = new ArrayCollection;
		$this->holidays = new ArrayCollection;
		$this->temporaryStops = new ArrayCollection;
		$this->pregeneratedOpenings = new ArrayCollection;
	}

}
