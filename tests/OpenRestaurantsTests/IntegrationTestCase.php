<?php

namespace OpenRestaurantsTests;

use Kdyby\Doctrine\Helpers;
use Nette\Configurator;
use Nette\DI\Container;
use Tester\TestCase;
use Tester\Helpers as TesterHelpers;

abstract class IntegrationTestCase extends TestCase
{

	/** @var Container|\SystemContainer */
	private $container;

	/**
	 * @return Container|\SystemContainer
	 */
	public function getContainer()
	{
		if ($this->container === NULL) {
			$this->container = $this->createContainer();
		}
		return $this->container;
	}

	private function createContainer()
	{
		$tempDir = __DIR__ . '/../temp/' . getmypid();
		TesterHelpers::purge($tempDir);
		@chmod($tempDir, 0777);
		$configurator = new Configurator;
		$configurator->enableDebugger(__DIR__ . '/../log');
		$configurator->setTempDirectory($tempDir);
		$configurator->addConfig(__DIR__ . '/../../app/config/config.neon');
		$configurator->addConfig(__DIR__ . '/../../app/config/config.local.neon');
		$configurator->addParameters([
			'appDir' => __DIR__ . '/../',
		]);
		$container = $configurator->createContainer();

		return $container;
	}

	/**
	 * Switch the default connection to a newly imported database with testing data.
	 *
	 * Copy-pasted from Kdyby/TesterExtras.
	 *
	 * @see https://github.com/Kdyby/TesterExtras/blob/master/src/Bootstrap.php#L72-L92
	 * @return string
	 */
	protected function setupDoctrineDatabase()
	{
		$sl = $this->getContainer();
		$sqls = [
			__DIR__ . '/../../structure.sql',
			__DIR__ . '/../../test-data.sql',
		];

		$db = $sl->getByType('Kdyby\Doctrine\Connection'); // default connection
		/** @var \Kdyby\Doctrine\Connection $db */

		$testDbName = 'open_restaurants_test_' . getmypid();
		$db->exec("DROP DATABASE IF EXISTS `$testDbName`");
		$db->exec("CREATE DATABASE `$testDbName`");
		$db->exec("USE `$testDbName`");

		foreach ($sqls as $file) {
			Helpers::loadFromFile($db, $file);
		}

		// drop on shutdown
		register_shutdown_function(function () use ($db, $testDbName) {
			$db->exec("DROP DATABASE IF EXISTS `$testDbName`");
		});

		return $testDbName;
	}

}
