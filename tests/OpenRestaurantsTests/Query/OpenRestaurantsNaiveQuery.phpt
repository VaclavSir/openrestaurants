<?php

namespace OpenRestaurantsTests\Query;

use Kdyby\Doctrine\QueryObject;
use OpenRestaurants;

require_once __DIR__ . '/../../bootstrap.php';

class OpenRestaurantsNaiveQuery extends OpenRestaurantsQueryTestCase
{

	/**
	 * @param \DateTime $currentTime
	 * @return QueryObject
	 */
	protected function createQuery(\DateTime $currentTime)
	{
		return new OpenRestaurants\Query\OpenRestaurantsNaiveQuery($currentTime);
	}

}

(new OpenRestaurantsNaiveQuery)->run();
