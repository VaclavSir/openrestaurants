<?php

namespace OpenRestaurantsTests\Query;

use Kdyby\Doctrine\EntityDao;
use Kdyby\Doctrine\EntityManager;
use OpenRestaurants;
use OpenRestaurantsTests\IntegrationTestCase;
use Tester\Assert;

require_once __DIR__ . '/../../bootstrap.php';

class OpeningHoursQuery extends IntegrationTestCase
{

	/** @var EntityManager */
	private $em;

	/** @var EntityDao */
	private $openingHoursDao;

	protected function setUp()
	{
		parent::setUp();
		$this->setupDoctrineDatabase();
		$this->em = $this->getContainer()->getByType(EntityManager::class);
		$this->openingHoursDao = $this->em->getRepository(OpenRestaurants\OpeningHours::class);
	}

	public function testLoadsAllByDefault()
	{
		$query = new OpenRestaurants\Query\OpeningHoursQuery;
		$openingHours = $this->openingHoursDao->fetch($query);
		Assert::count(26, $openingHours);
	}

	public function testSetDayOfWeek()
	{
		$query = new OpenRestaurants\Query\OpeningHoursQuery;
		$query->setDayOfWeek(2);
		$openingHours = $this->openingHoursDao->fetch($query);
		Assert::count(4, $openingHours);
	}

	public function testSetRestaurant()
	{
		$query = new OpenRestaurants\Query\OpeningHoursQuery;
		$query->setRestaurant($this->em->getReference(OpenRestaurants\Restaurant::class, 1));
		$openingHours = $this->openingHoursDao->fetch($query);
		Assert::count(10, $openingHours);
	}

	public function testSetDayOfWeekAndRestaurant()
	{
		$query = new OpenRestaurants\Query\OpeningHoursQuery;
		$query->setDayOfWeek(2);
		$query->setRestaurant($this->em->getReference(OpenRestaurants\Restaurant::class, 1));
		$openingHours = $this->openingHoursDao->fetch($query);
		Assert::count(2, $openingHours);
	}

}

(new OpeningHoursQuery)->run();
