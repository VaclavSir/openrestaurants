<?php

namespace OpenRestaurantsTests\Query;

use Kdyby\Doctrine\EntityDao;
use Kdyby\Doctrine\EntityManager;
use OpenRestaurants;
use OpenRestaurantsTests\IntegrationTestCase;
use Tester\Assert;

require_once __DIR__ . '/../../bootstrap.php';

class TemporaryStopQuery extends IntegrationTestCase
{

	/** @var EntityManager */
	private $em;

	/** @var EntityDao */
	private $temporaryStopDao;

	protected function setUp()
	{
		parent::setUp();
		$this->setupDoctrineDatabase();
		$this->em = $this->getContainer()->getByType(EntityManager::class);
		$this->temporaryStopDao = $this->em->getRepository(OpenRestaurants\TemporaryStop::class);
	}

	public function testLoadsAllByDefault()
	{
		$query = new OpenRestaurants\Query\TemporaryStopQuery;
		$temporaryStops = $this->temporaryStopDao->fetch($query);
		Assert::count(3, $temporaryStops);
	}

	public function testSetDate()
	{
		$query = new OpenRestaurants\Query\TemporaryStopQuery;
		$query->setDate(new \DateTime('2015-02-04'));
		$temporaryStops = $this->temporaryStopDao->fetch($query);
		Assert::count(1, $temporaryStops);
	}

	public function testSetRestaurant()
	{
		$query = new OpenRestaurants\Query\TemporaryStopQuery;
		$query->setRestaurant($this->em->getReference(OpenRestaurants\Restaurant::class, 3));
		$temporaryStops = $this->temporaryStopDao->fetch($query);
		Assert::count(2, $temporaryStops);
	}

	public function testSetDateAndRestaurant()
	{
		$query = new OpenRestaurants\Query\TemporaryStopQuery;
		$query->setDate(new \DateTime('2015-02-28'));
		$query->setRestaurant($this->em->getReference(OpenRestaurants\Restaurant::class, 3));
		$temporaryStops = $this->temporaryStopDao->fetch($query);
		Assert::count(1, $temporaryStops);
	}

}

(new TemporaryStopQuery)->run();
