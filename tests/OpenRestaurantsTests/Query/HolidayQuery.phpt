<?php

namespace OpenRestaurantsTests\Query;

use Kdyby\Doctrine\EntityDao;
use Kdyby\Doctrine\EntityManager;
use OpenRestaurants;
use OpenRestaurantsTests\IntegrationTestCase;
use Tester\Assert;

require_once __DIR__ . '/../../bootstrap.php';

class HolidayQuery extends IntegrationTestCase
{

	/** @var EntityManager */
	private $em;

	/** @var EntityDao */
	private $holidaysDao;

	protected function setUp()
	{
		parent::setUp();
		$this->setupDoctrineDatabase();
		$this->em = $this->getContainer()->getByType(EntityManager::class);
		$this->holidaysDao = $this->em->getRepository(OpenRestaurants\Holiday::class);
	}

	public function testLoadsAllByDefault()
	{
		$query = new OpenRestaurants\Query\HolidayQuery;
		$holidays = $this->holidaysDao->fetch($query);
		Assert::count(3, $holidays);
	}

	public function testSetDate()
	{
		$query = new OpenRestaurants\Query\HolidayQuery;
		$query->setDate(new \DateTime('2015-02-03'));
		$holidays = $this->holidaysDao->fetch($query);
		Assert::count(2, $holidays);
	}

	public function testSetRestaurant()
	{
		$query = new OpenRestaurants\Query\HolidayQuery;
		$query->setRestaurant($this->em->getReference(OpenRestaurants\Restaurant::class, 3));
		$holidays = $this->holidaysDao->fetch($query);
		Assert::count(1, $holidays);
	}

	public function testSetDateAndRestaurant()
	{
		$query = new OpenRestaurants\Query\HolidayQuery;
		$query->setDate(new \DateTime('2015-02-03'));
		$query->setRestaurant($this->em->getReference(OpenRestaurants\Restaurant::class, 3));
		$holidays = $this->holidaysDao->fetch($query);
		Assert::count(1, $holidays);
	}

}

(new HolidayQuery)->run();
