<?php

namespace OpenRestaurantsTests\Query;

use Kdyby\Doctrine\EntityManager;
use Kdyby\Doctrine\QueryObject;
use OpenRestaurants;
use OpenRestaurants\Pregenerating\Generator;
use OpenRestaurants\Restaurant;

require_once __DIR__ . '/../../bootstrap.php';

class OpenRestaurantsOptimizedQuery extends OpenRestaurantsQueryTestCase
{
	protected function setUp()
	{
		parent::setUp();
		/** @var EntityManager $em */
		$em = $this->getContainer()->getByType(EntityManager::class);
		$restaurants = $em->getRepository(Restaurant::class)->findAll();
		/** @var Generator $generator */
		$generator = $this->getContainer()->getByType(Generator::class);
		$generator->generate($restaurants, new \DateTime('2015-02-01'), new \DateTime('2015-02-05'));
	}

	/**
	 * @param \DateTime $currentTime
	 * @return QueryObject
	 */
	protected function createQuery(\DateTime $currentTime)
	{
		return new OpenRestaurants\Query\OpenRestaurantsOptimizedQuery($currentTime);
	}

}

(new OpenRestaurantsOptimizedQuery)->run();
