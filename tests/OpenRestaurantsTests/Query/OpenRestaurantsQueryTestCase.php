<?php

namespace OpenRestaurantsTests\Query;

use Kdyby\Doctrine\EntityDao;
use Kdyby\Doctrine\EntityManager;
use Kdyby\Doctrine\QueryObject;
use OpenRestaurants;
use OpenRestaurantsTests\IntegrationTestCase;
use Tester\Assert;

abstract class OpenRestaurantsQueryTestCase extends IntegrationTestCase
{

	/** @var EntityDao */
	private $restaurantsDao;

	protected function setUp()
	{
		parent::setUp();
		$this->setupDoctrineDatabase();
		/** @var EntityManager $em */
		$em = $this->getContainer()->getByType(EntityManager::class);
		$this->restaurantsDao = $em->getRepository(OpenRestaurants\Restaurant::class);
	}

	/**
	 * Monday 2015-02-02 10:00:00
	 *
	 * Open according to opening hours: #1, #2, #3, #4
	 */
	public function testOpeningHours_1()
	{
		$currentTime = new \DateTime('2015-02-02 10:00:00');
		$openRestaurantIds = $this->fetchOpenRestaurantIds($currentTime);
		Assert::same([1, 2, 3, 4], $openRestaurantIds);
	}

	/**
	 * Monday 2015-02-02 7:30:00
	 *
	 * Open according to opening hours: #2, #4
	 */
	public function testOpeningHours_2()
	{
		$currentTime = new \DateTime('2015-02-02 7:30:00');
		$openRestaurantIds = $this->fetchOpenRestaurantIds($currentTime);
		Assert::same([2, 4], $openRestaurantIds);
	}

	/**
	 * Tuesday 2015-02-03 10:00:00
	 *
	 * Open according to opening hours: #1, #2, #3
	 * Closed due to holidays: #2, #3
	 */
	public function testHoliday()
	{
		$currentTime = new \DateTime('2015-02-03 10:00:00');
		$openRestaurantIds = $this->fetchOpenRestaurantIds($currentTime);
		Assert::same([1], $openRestaurantIds);
	}

	/**
	 * Wednesday 2015-02-04 13:00:00
	 *
	 * Open according to opening hours: #1, #2, #3
	 * Temporary stopped: #1
	 */
	public function testTemporaryStop()
	{
		$currentTime = new \DateTime('2015-02-04 13:00:00');
		$openRestaurantIds = $this->fetchOpenRestaurantIds($currentTime);
		Assert::same([2, 3], $openRestaurantIds);
	}

	private function fetchOpenRestaurantIds(\DateTime $currentTime)
	{
		$query = $this->createQuery($currentTime);
		$openRestaurants = $this->restaurantsDao->fetch($query);
		$openRestaurantIds = array_map(function (OpenRestaurants\Restaurant $restaurant) {
			return $restaurant->id;
		}, $openRestaurants->toArray());
		sort($openRestaurantIds);
		return $openRestaurantIds;
	}

	/**
	 * @param \DateTime $currentTime
	 * @return QueryObject
	 */
	protected function createQuery(\DateTime $currentTime)
	{
		return new OpenRestaurants\Query\OpenRestaurantsNaiveQuery($currentTime);
	}

}
