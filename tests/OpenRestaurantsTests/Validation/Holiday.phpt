<?php

namespace OpenRestaurantsTests\Validation;

use OpenRestaurants;
use Tester\Assert;

require_once __DIR__ . '/../../bootstrap.php';

class Holiday extends ValidationTestCase
{

	public function testDayBlank()
	{
		$holiday = new OpenRestaurants\Holiday;
		$violations = $this->validateProperty($holiday, 'day');
		Assert::count(1, $violations);
	}

	public function testDayValid()
	{
		$holiday = new OpenRestaurants\Holiday;
		$holiday->day = new \DateTime;
		$violations = $this->validateProperty($holiday, 'day');
		Assert::count(0, $violations);
	}

}

(new Holiday)->run();
