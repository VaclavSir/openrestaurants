<?php

namespace OpenRestaurantsTests\Validation;

use OpenRestaurants;
use Tester\Assert;

require_once __DIR__ . '/../../bootstrap.php';

class TemporaryStop extends ValidationTestCase
{

	public function testStartBlank()
	{
		$stop = new OpenRestaurants\TemporaryStop;
		$violations = $this->validateProperty($stop, 'start');
		Assert::count(1, $violations);
	}

	public function testStartValid()
	{
		$stop = new OpenRestaurants\TemporaryStop;
		$stop->start = new \DateTime;
		$violations = $this->validateProperty($stop, 'start');
		Assert::count(0, $violations);
	}

	public function testStopBlank()
	{
		$stop = new OpenRestaurants\TemporaryStop;
		$violations = $this->validateProperty($stop, 'stop');
		Assert::count(1, $violations);
	}

	public function testStopValid()
	{
		$stop = new OpenRestaurants\TemporaryStop;
		$stop->stop = new \DateTime;
		$violations = $this->validateProperty($stop, 'stop');
		Assert::count(0, $violations);
	}

}

(new TemporaryStop)->run();
