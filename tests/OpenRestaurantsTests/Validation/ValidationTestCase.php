<?php

namespace OpenRestaurantsTests\Validation;

use OpenRestaurantsTests\IntegrationTestCase;
use Symfony\Component\Validator\ValidatorInterface;

abstract class ValidationTestCase extends IntegrationTestCase
{

	/**
	 * @param object $entity
	 * @param string $propertyName
	 * @return \Symfony\Component\Validator\ConstraintViolationListInterface
	 */
	protected function validateProperty($entity, $propertyName)
	{
		/** @var ValidatorInterface $validator */
		$validator = $this->getContainer()->getByType(ValidatorInterface::class);
		$violations = $validator->validateProperty($entity, $propertyName);
		return $violations;
	}

}
