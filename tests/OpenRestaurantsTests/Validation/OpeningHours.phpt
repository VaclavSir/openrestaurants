<?php

namespace OpenRestaurantsTests\Validation;

use OpenRestaurants;
use Tester\Assert;

require_once __DIR__ . '/../../bootstrap.php';

class OpeningHours extends ValidationTestCase
{

	public function testDayOfWeekBlank()
	{
		$openingHours = new OpenRestaurants\OpeningHours;
		$violations = $this->validateProperty($openingHours, 'dayOfWeek');
		Assert::count(1, $violations);
	}

	public function testDayOfWeekOutOfRange()
	{
		$openingHours = new OpenRestaurants\OpeningHours;
		$openingHours->dayOfWeek = 0;
		$violations = $this->validateProperty($openingHours, 'dayOfWeek');
		Assert::count(1, $violations);
		$openingHours->dayOfWeek = 8;
		$violations = $this->validateProperty($openingHours, 'dayOfWeek');
		Assert::count(1, $violations);
	}

	/**
	 * @dataProvider getValidDaysOfWeek
	 */
	public function testDayOfWeekValid($validDay)
	{
		$openingHours = new OpenRestaurants\OpeningHours;
		$openingHours->dayOfWeek = $validDay;
		$violations = $this->validateProperty($openingHours, 'dayOfWeek');
		Assert::count(0, $violations);
	}

	public function getValidDaysOfWeek()
	{
		return [[1], [2], [3], [4], [5], [6], [7]];
	}

	public function testStartBlank()
	{
		$openingHours = new OpenRestaurants\OpeningHours;
		$violations = $this->validateProperty($openingHours, 'start');
		Assert::count(1, $violations);
	}

	public function testStartValid()
	{
		$openingHours = new OpenRestaurants\OpeningHours;
		$openingHours->start = new \DateTime;
		$violations = $this->validateProperty($openingHours, 'start');
		Assert::count(0, $violations);
	}

	public function testStopBlank()
	{
		$openingHours = new OpenRestaurants\OpeningHours;
		$violations = $this->validateProperty($openingHours, 'stop');
		Assert::count(1, $violations);
	}

	public function testStopValid()
	{
		$openingHours = new OpenRestaurants\OpeningHours;
		$openingHours->stop = new \DateTime;
		$violations = $this->validateProperty($openingHours, 'stop');
		Assert::count(0, $violations);
	}

}

(new OpeningHours)->run();
