<?php

namespace OpenRestaurantsTests\Pregenerating;

use OpenRestaurants\Holiday;
use OpenRestaurants\OpeningHours;
use OpenRestaurants\Pregenerating\SingleDayGenerator as Generator;
use OpenRestaurants\Restaurant;
use OpenRestaurants\TemporaryStop;
use Tester\Assert;
use Tester\TestCase;

require_once __DIR__ . '/../../bootstrap.php';

class SingleDayGenerator extends TestCase
{

	public function testHolidayBlocksTheWholeDay()
	{
		$restaurant = new Restaurant;
		$openingHours = [
			$this->createOpeningHours('08:00:00', '18:00:00')
		];
		$holidays = [
			new Holiday
		];
		$temporaryStops = [];
		$openings = Generator::generateOpenings($restaurant, $openingHours, $holidays, $temporaryStops);
		Assert::same([], $openings);
	}

	public function testOpeningHours(){
		$restaurant = new Restaurant;
		$openingHours = [
			$this->createOpeningHours('08:00:00', '18:00:00'),
			$this->createOpeningHours('20:30:00', '23:00:00'),
		];
		$holidays = [];
		$temporaryStops = [];
		$openings = Generator::generateOpenings($restaurant, $openingHours, $holidays, $temporaryStops);

		Assert::count(2, $openings);

		Assert::same($restaurant, $openings[0]->restaurant);
		Assert::same('08:00:00', $openings[0]->start->format('H:i:s'));
		Assert::same('18:00:00', $openings[0]->stop->format('H:i:s'));

		Assert::same($restaurant, $openings[1]->restaurant);
		Assert::same('20:30:00', $openings[1]->start->format('H:i:s'));
		Assert::same('23:00:00', $openings[1]->stop->format('H:i:s'));
	}

	public function testOverlappingOpeningHours()
	{
		$restaurant = new Restaurant;
		$openingHours = [
			$this->createOpeningHours('08:00:00', '10:00:00'),
			$this->createOpeningHours('09:00:00', '12:00:00'),
			$this->createOpeningHours('12:00:00', '13:00:00'),
		];
		$holidays = [];
		$temporaryStops = [];
		$openings = Generator::generateOpenings($restaurant, $openingHours, $holidays, $temporaryStops);

		Assert::count(1, $openings);
		Assert::same($restaurant, $openings[0]->restaurant);
		Assert::same('08:00:00', $openings[0]->start->format('H:i:s'));
		Assert::same('13:00:00', $openings[0]->stop->format('H:i:s'));
	}

	public function testTemporaryStops()
	{
		$restaurant = new Restaurant;
		$openingHours = [
			$this->createOpeningHours('08:00:00', '18:00:00'),
			$this->createOpeningHours('20:30:00', '23:00:00'),
			$this->createOpeningHours('23:40:00', '23:50:00'),
		];
		$holidays = [];
		$temporaryStops = [
			$this->createTemporaryStop('10:30:00', '10:45:00'),
			$this->createTemporaryStop('22:55:00', '23:10:00'),
			$this->createTemporaryStop('23:40:00', '23:50:00'),
		];
		$openings = Generator::generateOpenings($restaurant, $openingHours, $holidays, $temporaryStops);

		Assert::count(3, $openings);

		Assert::same($restaurant, $openings[0]->restaurant);
		Assert::same('08:00:00', $openings[0]->start->format('H:i:s'));
		Assert::same('10:30:00', $openings[0]->stop->format('H:i:s'));

		Assert::same($restaurant, $openings[1]->restaurant);
		Assert::same('10:45:00', $openings[1]->start->format('H:i:s'));
		Assert::same('18:00:00', $openings[1]->stop->format('H:i:s'));

		Assert::same($restaurant, $openings[2]->restaurant);
		Assert::same('20:30:00', $openings[2]->start->format('H:i:s'));
		Assert::same('22:55:00', $openings[2]->stop->format('H:i:s'));
	}

	public function testTemporaryStopDoesntCrashOnClosingDay()
	{
		$restaurant = new Restaurant;
		$openingHours = [];
		$holidays = [];
		$temporaryStops = [
			$this->createTemporaryStop('10:30:00', '10:45:00'),
		];
		$openings = Generator::generateOpenings($restaurant, $openingHours, $holidays, $temporaryStops);

		Assert::count(0, $openings);
	}

	public function testTemporaryStopCanOverlapOpeningHours()
	{
		$restaurant = new Restaurant;
		$openingHours = [
			$this->createOpeningHours('11:00:00', '13:00:00'),
		];
		$holidays = [];
		$temporaryStops = [
			$this->createTemporaryStop('10:00:00', '14:00:00'),
		];
		$openings = Generator::generateOpenings($restaurant, $openingHours, $holidays, $temporaryStops);

		Assert::count(0, $openings);
	}

	/**
	 * @param string $start
	 * @param string $stop
	 * @return OpeningHours
	 */
	private function createOpeningHours($start, $stop)
	{
		$openingHours = new OpeningHours;
		$openingHours->start = new \DateTime($start);
		$openingHours->stop = new \DateTime($stop);
		return $openingHours;
	}

	/**
	 * @param string $start
	 * @param string $stop
	 * @return TemporaryStop
	 */
	private function createTemporaryStop($start, $stop)
	{
		$temporaryStop = new TemporaryStop;
		$temporaryStop->start = new \DateTime($start);
		$temporaryStop->stop = new \DateTime($stop);
		return $temporaryStop;
	}

}

(new SingleDayGenerator)->run();
