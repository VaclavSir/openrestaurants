<?php

namespace OpenRestaurantsTests\Pregenerating;

use Kdyby\Doctrine\EntityManager;
use OpenRestaurants\PregeneratedOpening;
use OpenRestaurants\Restaurant;
use OpenRestaurantsTests\IntegrationTestCase;
use OpenRestaurants\Pregenerating;
use Tester\Assert;

require_once __DIR__ . '/../../bootstrap.php';

class Generator extends IntegrationTestCase
{

	/** @var Pregenerating\Generator */
	private $generator;

	/** @var EntityManager */
	private $em;

	public function setup(){
		$this->setupDoctrineDatabase();
		$this->em = $this->getContainer()->getByType(EntityManager::class);
		$this->generator = new Pregenerating\Generator($this->em);
	}

	public function testEmptyByDefault()
	{
		$openings = $this->em->getRepository(PregeneratedOpening::class)->findAll();
		Assert::count(0, $openings);
	}

	/**
	 * Wednesday 2015-02-04, restaurant #1
	 *
	 * Opening hours:
	 *
	 * - 08:00 - 11:00
	 * - 12:00 - 18:00
	 *
	 * Temporary stopped:
	 *
	 * - 13:00 - 13:10
	 */
	public function testGenerateOneRestaurantOneDay()
	{
		$restaurant = $this->em->find(Restaurant::class, 1);

		$this->generator->generate([$restaurant], new \DateTime('2015-02-04'), new \DateTime('2015-02-04'));

		$openings = $this->em->getRepository(PregeneratedOpening::class)->findBy([], ['start' => 'DESC']);
		Assert::count(3, $openings);
	}

	public function testRepeatedRunDeletesOldData()
	{
		$restaurant = $this->em->find(Restaurant::class, 1);

		$this->generator->generate([$restaurant], new \DateTime('2015-02-04'), new \DateTime('2015-02-04'));
		$this->generator->generate([$restaurant], new \DateTime('2015-02-04'), new \DateTime('2015-02-04'));

		$openings = $this->em->getRepository(PregeneratedOpening::class)->findBy([], ['start' => 'DESC']);
		Assert::count(3, $openings);
	}

	public function testRepeatedRunDoesntDeleteDataForOtherRestaurants()
	{
		$restaurant = $this->em->find(Restaurant::class, 2);
		$this->generator->generate([$restaurant], new \DateTime('2015-02-04'), new \DateTime('2015-02-04'));
		$openings = $this->em->getRepository(PregeneratedOpening::class)->findBy([], ['start' => 'DESC']);
		Assert::count(1, $openings);

		$restaurant = $this->em->find(Restaurant::class, 1);
		$this->generator->generate([$restaurant], new \DateTime('2015-02-04'), new \DateTime('2015-02-04'));
		$openings = $this->em->getRepository(PregeneratedOpening::class)->findBy([], ['start' => 'DESC']);
		Assert::count(4, $openings);
	}

	public function testRepeatedRunDoesntDeleteOtherDays()
	{
		$restaurant = $this->em->find(Restaurant::class, 1);
		$this->generator->generate([$restaurant], new \DateTime('2015-02-01'), new \DateTime('2015-02-04'));
		$openings = $this->em->getRepository(PregeneratedOpening::class)->findBy([], ['start' => 'DESC']);
		Assert::count(7, $openings);

		$this->generator->generate([$restaurant], new \DateTime('2015-02-03'), new \DateTime('2015-02-03'));
		Assert::count(7, $openings);
	}

}

(new Generator)->run();
