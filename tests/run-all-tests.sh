#!/bin/sh

BASEDIR=$(dirname $0)
$BASEDIR/../vendor/bin/tester \
	-p /usr/bin/php \
	-c $BASEDIR/php-unix.ini \
	--colors 1 \
	$BASEDIR
