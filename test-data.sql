DELIMITER //

CREATE PROCEDURE tmp_add_opening_hours(_restaurant_id INT, _day_of_week TINYINT, _start TIME, _stop TIME)
  BEGIN
    INSERT INTO opening_hours (restaurant_id, day_of_week, start, stop) VALUES (_restaurant_id, _day_of_week, _start, _stop);
  END//

DELIMITER ;


INSERT INTO restaurant VALUES (1);
CALL tmp_add_opening_hours(1, 1, '08:00:00', '11:00:00');
CALL tmp_add_opening_hours(1, 1, '12:00:00', '18:00:00');
CALL tmp_add_opening_hours(1, 2, '08:00:00', '11:00:00');
CALL tmp_add_opening_hours(1, 2, '12:00:00', '18:00:00');
CALL tmp_add_opening_hours(1, 3, '08:00:00', '11:00:00');
CALL tmp_add_opening_hours(1, 3, '12:00:00', '18:00:00');
CALL tmp_add_opening_hours(1, 4, '08:00:00', '11:00:00');
CALL tmp_add_opening_hours(1, 4, '12:00:00', '18:00:00');
CALL tmp_add_opening_hours(1, 5, '08:00:00', '11:00:00');
CALL tmp_add_opening_hours(1, 5, '12:00:00', '18:00:00');

INSERT INTO restaurant VALUES (2);
CALL tmp_add_opening_hours(2, 1, '06:00:00', '22:00:00');
CALL tmp_add_opening_hours(2, 2, '06:00:00', '22:00:00');
CALL tmp_add_opening_hours(2, 3, '06:00:00', '22:00:00');
CALL tmp_add_opening_hours(2, 4, '06:00:00', '22:00:00');
CALL tmp_add_opening_hours(2, 5, '06:00:00', '22:00:00');
CALL tmp_add_opening_hours(2, 6, '06:00:00', '22:00:00');
CALL tmp_add_opening_hours(2, 7, '06:00:00', '22:00:00');

INSERT INTO restaurant VALUES (3);
CALL tmp_add_opening_hours(3, 1, '10:00:00', '15:00:00');
CALL tmp_add_opening_hours(3, 2, '10:00:00', '15:00:00');
CALL tmp_add_opening_hours(3, 3, '10:00:00', '15:00:00');
CALL tmp_add_opening_hours(3, 4, '10:00:00', '15:00:00');
CALL tmp_add_opening_hours(3, 5, '10:00:00', '15:00:00');
CALL tmp_add_opening_hours(3, 6, '11:00:00', '16:00:00');

INSERT INTO restaurant VALUES (4);
CALL tmp_add_opening_hours(4, 1, '07:15:00', '08:45:00');
CALL tmp_add_opening_hours(4, 1, '09:15:00', '10:45:00');
CALL tmp_add_opening_hours(4, 1, '11:15:00', '12:45:00');

INSERT INTO holiday (restaurant_id, day) VALUES (2, '2015-02-03');
INSERT INTO holiday (restaurant_id, day) VALUES (3, '2015-02-03');
INSERT INTO holiday (restaurant_id, day) VALUES (1, '2015-02-10');

INSERT INTO temporary_stop (restaurant_id, start, stop) VALUES (1, '2015-02-04 13:00:00', '2015-02-04 13:10:00');
INSERT INTO temporary_stop (restaurant_id, start, stop) VALUES (3, '2015-02-27 13:00:00', '2015-02-27 13:10:00');
INSERT INTO temporary_stop (restaurant_id, start, stop) VALUES (3, '2015-02-28 13:00:00', '2015-02-28 13:10:00');

DROP PROCEDURE tmp_add_opening_hours;
